
$(function () {
    manage_meeting_scroll();
    init();
    /*Initialize Manage Meeting Tile View Carousel*/
    //init_tile_carousel();
    /*Re-Initialize the carousel again on window resize*/
    $(window).resize(function () {
        //clearTimeout(timeout);
        //reinit_carousel();
    });
    /* Add Document popup scroll*/
    document_popup_scroll();

    //$(document).on("click", ".meeting-view-btn", function () {
       
    //    var el = $(this);
    //    var view_el = el.data("view");
    //    if (!el.hasClass("active")) {
    //        $(".meeting-view-wrap.enabled").fadeOut(300, function () {
    //            $(view_el).fadeIn(400);
    //            $(".meeting-view-wrap.enabled").removeClass("enabled");
    //            $(view_el).addClass("enabled");
              
    //        });
    //        if (view_el == ".list-view-wrap") {
    //            $(".manage-meeting-wrap").addClass("list-view");
             
    //        }
    //        else {
    //            $(".manage-meeting-wrap").removeClass("list-view");
    //            //reinit_carousel()
               
    //        }
    //        $(".meeting-view-btn").removeClass("active");
    //        el.addClass("active");
           
    //    }
    //});



    /*To make upload notes button active*/
    $(document).on("input", ".notes-textarea", function () {
        var value = $(".notes-textarea").val();
        if (value.length > 0) {
            $(".upload-notes-btn").addClass("active");
        }
        else {
            $(".upload-notes-btn").removeClass("active");
        }
    });
    /* Upload note */
    //$(document).on("click", ".upload-notes-btn.active", function () {
    //    $(".notes-textarea").fadeOut(300, function () {
    //        $(".add-notes-text-wrap").height(60);
    //        $(".note-uploaded").fadeIn(300);
    //        $(".upload-notes-btn").fadeOut(300, function () {
    //            $(".view-notes-btn").fadeIn(300);
    //            $(".upload-notes-btn").removeClass("active");
    //            $(".add-another-note").fadeIn(300);
    //            $(".add-another-note").addClass("active");
    //        });

    //        $(".view-notes-btn").addClass("active");

    //    });
    //});
    /* To make upload attachment button active*/
    $(document).on("change", ".add-doc-file", function () {
        var value = $(this).prop('files')[0];
        console.log(value);
        if (value) {
            //$(".upload-doc-btn").addClass("active");
            $(this).siblings(".doc-input-cover").text(value.name);
        }
    });
    /* Upload attachment*/
    //$(document).on("click", ".upload-doc-btn.active", function () {

    //    $(".add-doc-input-wrap").fadeOut(300, function () {
    //        $(".add-doc-text-wrap").height(60);
    //        $(".doc-uploaded").fadeIn(300);
    //        $(".upload-doc-btn").fadeOut(300, function () {
    //            $(".view-doc-btn").fadeIn(300);
    //            $(".upload-doc-btn").removeClass("active");

    //        });

    //        $(".view-doc-btn").addClass("active");

    //    });
    //});
    $(document).on("click", ".add-notesordoc-popup .close-popup,.view-notes-btn.active,.add-another-note.active, .add-notesordoc-popup .close-popup ", function () {

      
        /*notes and attachment revers eng code*/
        $(".view-doc-btn").fadeOut(300);
        $(".doc-uploaded").fadeOut(300, function () {
            $("add-doc-text-wrap").height('auto');
            $(".add-doc-input-wrap").fadeIn(300);

            $(".upload-doc-btn").fadeOut(300, function () {
                $(".upload-doc-btn").fadeIn(300);
                $(".upload-doc-btn").removeClass("active");

            });
            $(".add-another-doc").removeClass("active");

        });
    });

    /* Add note and attachment Tab click*/
    $(document).on("click", ".add-popup-tab", function () {
        var el = $(this)
        // alert('el');
        var target = el.data("view");
        var common = $(".add-popup-tab");
        var target_common = $(".add-notesordoc-content.enabled");
        change_tabs(el, common, target, target_common);
    });

    /* Document main Tab click */
    $(document).on("click", ".document-popup-tab", function () {
        var el = $(this)
        var target = el.data("view");
        var common = $(".document-popup-tab");
        var target_common = $(".document-popup-content-wrap.enabled");
        if (target) {
            change_tabs(el, common, target, target_common);
        }

    });


    /* Document main Tab click */
    $(document).on("click", ".resource-popup-tab.valid", function () {
        var el = $(this)
        var target = el.data("view");
        var common = $(".resource-popup-tab");
        var target_common = $(".resource-popup-content-wrap.enabled");
        if (target) {
            change_tabs(el, common, target, target_common);
        }

    });
    $(document).on("click", ".cp-popup-tab", function () {
        var el = $(this)
        var target = el.data("view");
        var common = $(".cp-popup-tab");
        var target_common = $(".cp-popup-content-wrap.enabled");
        if (target) {
            change_tabs(el, common, target, target_common);
        }

    });


    /* Add notes Tab click*/
    $(document).on("click", ".note-tab-list", function () {
       // alert("under click");
        var el = $(this)
        console.log(el);
        var target = el.data("tab");
        var common = $(".note-tab-list");
        var target_common = $(".note-tab-box.enabled");
        if (target) {
            change_tabs(el, common, target, target_common);
        }

    });

    /* Add notes Tab click*/
    //$(document).on("click", ".note-tab-list1", function () {
    //    alert
    //    var el = $(this)
    //    var target = el.data("tab");
    //    var common = $(".note-tab-list1");
    //    var target_common = $(".note-tab-box.enabled");
    //    if (target) {
    //        change_tabs(el, common, target, target_common);
    //    }

    //});

    /* Accordion functionality*/
    $(document).on('click', ".ss-button", function () {
        var el = $(this);
        if (el.next('.ss-content').is(":visible")) {
            $('.ss-content.enabled').slideUp(200, function () {
                $('.ss-button .accordion-title').removeClass("active");
                $('.ss-content').removeClass("enabled");
            });

        } else {
            $('.ss-content.enabled').slideUp(200, function () {

            });

            $('.ss-content').removeClass("enabled");
            $('.ss-button .accordion-title').removeClass("active");
            el.find(".accordion-title").addClass("active");
            el.next('.ss-content').addClass("enabled");
            el.next('.ss-content').slideDown(200);

        }
    });


 



    /*Open Add popup*/
    //$(document).on("click", ".action-add-btn", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /*Open Add notes and attachmentpopup*/
    $(document).on("click", ".action-document-btn", function () {
        var el = $(this).data("popup");
        popup_open(el);
    });
    /*Open popups from add popup page*/
    $(document).on("click", ".add-popup-list", function () {
        var el = $(this).data("popup");
        //  alert(el);
        popup_open(el);
    });
    /*Open popups from edit popup task*/
    //$(document).on("click", ".edit-task-btn", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});

    /*Close popup trigger*/
    $(document).on("click", ".k-popup .close-popup", function () {

        popup_close();
    });

    /* Notification popup open*/

    //$(document).on("click", ".notification-btn.active", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /* Meeting following detail popup open*/

    //$(document).on("click", ".following-status", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});

    /* Meeting follower detail popup open*/

    $(document).on("click", ".action-following-btn", function () {
        var el = $(this).data("popup");
        popup_open(el);
    });

    /* Delete meeting popup open*/

    //    $(document).on("click", ".action-delete-btn", function () {
    //        var el = $(this).data("popup");
    //        popup_open(el);
    //    });
    //});

    /* Add Document popup scroll*/
    function document_popup_scroll() {
        $(".document-popup .popup-content").perfectScrollbar();
    }
    /*Change add notes and attachment tabs*/
    function change_tabs(el, common, target, target_common, callback) {
        if (!el.hasClass("active")) {
            $(target_common).fadeOut(300, function () {
                $(target).fadeIn(300);
                $(target_common).removeClass("enabled");
                $(target).addClass("enabled");
            });
            $(common).removeClass("active");
            el.addClass("active");
            if (callback) {
                callbak();
            }
        }
    }

    function manage_meeting_scroll() {
        $(".manage-meeting-wrap").niceScroll();
    }
    /*Function Initialize Manage Meeting Tile View Carousel*/
    //function init_tile_carousel() {
    //    console.log($('.tile-view-wrap'));

    //    $('.tile-view-wrap').slick({
    //        infinite: false,
    //        slidesToShow: 1,
    //        slidesToScroll: 1,
    //        dots: true,
    //        speed: 300,
    //        arrows: false
    //    });


    //}
    //function reinit_carousel() {
    //    var $tile_slider = $('.tile-view-wrap');
    //    var timeout = setTimeout(function () {
    //        $tile_slider.slick('slickGoTo', 0);
    //    }, 100);
    //}
    /* Things to be initialized*/
    function init() {
        /* Initilization for popup inner elements animation*/

        TweenMax.set($(".k-popup").find(".anim-lr"), {
            opacity: 0,
            x: 50
        });
        TweenMax.set($(".k-popup").find(".anim-bt"), {
            opacity: 0,
            y: 50
        });
        TweenMax.set($(".k-popup").find(".anim-tb"), {
            opacity: 0,
            y: -50
        });
        TweenMax.set(".popup-cover", {
            x: win_w

        });
    }


    /* Popup Open function */
    function popup_inner_open(el) {
        $(el).fadeIn(400);
    }
    function popup_inner_close(el) {
        $(".k-popup-2").fadeOut(400);
    }
    function popup_open(el) {

        TweenMax.to(".popup-cover", 0.4, {
            x: 0,
            display: "block",
            force3D: !0,
            ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
            onComplete: function () {
                TweenMax.to(".popup-cover", 0.7, {
                    x: -win_w,
                    force3D: !0,
                    ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
                    onStart: function () {
                        $(".k-popup").hide();
                        TweenMax.set($(".k-popup").find(".anim-lr"), {
                            opacity: 0,
                            x: 50
                        });
                        TweenMax.set($(".k-popup").find(".anim-bt"), {
                            opacity: 0,
                            y: 50
                        });
                        TweenMax.set($(".k-popup").find(".anim-tb"), {
                            opacity: 0,
                            y: -50
                        });
                        $(el).show();
                        TweenMax.staggerTo($(el).find(".anim-tb"), 0.6, {
                            delay: 0.3,
                            y: 0,
                            opacity: 1,
                            force3D: !0,
                            ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                        }, 0.1);
                        TweenMax.staggerTo($(el).find(".anim-lr"), 0.6, {
                            delay: 0.3,
                            x: 0,
                            opacity: 1,
                            force3D: !0,
                            ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                        }, 0.1);
                        TweenMax.staggerTo($(el).find(".anim-bt"), 0.6, {
                            delay: 0.3,
                            y: 0,
                            opacity: 1,
                            force3D: !0,
                            ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                        }, 0.1);
                    },
                    onComplete: function () {

                        TweenMax.set(".popup-cover", {
                            x: win_w,
                            display: "none"
                        });
                    }
                });
            }
        });
    }

   
    /* Popup Close function */
    function popup_close() {
        TweenMax.to(".popup-cover", 0.4, {
            x: 0,
            display: "block",
            force3D: !0,
            ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
            onComplete: function () {
                TweenMax.to(".popup-cover", 0.7, {
                    x: -win_w,
                    force3D: !0,
                    ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
                    onStart: function () {
                        $(".k-popup").hide();
                        TweenMax.set($(".k-popup").find(".anim-lr"), {
                            opacity: 0,
                            x: 50
                        });
                        TweenMax.set($(".k-popup").find(".anim-bt"), {
                            opacity: 0,
                            y: 50
                        });
                        TweenMax.set($(".k-popup").find(".anim-tb"), {
                            opacity: 0,
                            y: -50
                        });
                    },
                    onComplete: function () {

                        TweenMax.set(".popup-cover", {
                            x: win_w,
                            display: "none"
                        });
                    }
                });
            }
        });
    }
});

function goBack() {
    popup_close();
    window.history.back();
}

