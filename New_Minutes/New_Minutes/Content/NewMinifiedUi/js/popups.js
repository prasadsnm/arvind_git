$(function(){
    init();
    /* Add Document popup scroll*/
    document_popup_scroll();
    /*Close popup trigger*/
    $(document).on("click", ".close-popup",function(){
        
        popup_close();
    });

    /*To make upload notes button active*/
    $(document).on("input", ".notes-textarea",function(){
        var value = $(".notes-textarea").val();
        if(value.length > 0){
            $(".upload-notes-btn").addClass("active");
        }
        else{
            $(".upload-notes-btn").removeClass("active");
        }
    });
    /* Upload note */
    $(document).on("click", ".upload-notes-btn.active",function(){
        $(".notes-textarea").fadeOut(300, function(){
            $(".add-notes-text-wrap").height(60);
            $(".note-uploaded").fadeIn(300);
            $(".upload-notes-btn").fadeOut(300, function(){
                $(".view-notes-btn").fadeIn(300);
                $(".upload-notes-btn").removeClass("active");
                $(".add-another-note").fadeIn(300);
                $(".add-another-note").addClass("active");
            });
            
            $(".view-notes-btn").addClass("active");
            
        });
    });
    /* To make upload attachment button active*/
    $(document).on("change", ".add-doc-file", function(){
        var value = $(this).prop('files')[0];
        console.log(value);
        if(value){
             //$(".upload-doc-btn").addClass("active");
             $(this).siblings(".doc-input-cover").text(value.name);
        }
    });
    /* Upload attachment*/
    $(document).on("click", ".upload-doc-btn.active",function(){
        $(".add-doc-input-wrap").fadeOut(300, function(){
            $(".add-doc-text-wrap").height(60);
            $(".doc-uploaded").fadeIn(300);
            $(".upload-doc-btn").fadeOut(300, function(){
                $(".view-doc-btn").fadeIn(300);
                $(".upload-doc-btn").removeClass("active");
                
            });
            
            $(".view-doc-btn").addClass("active");
            
        });
    });

    /* Add note and attachment Tab click*/
    $(document).on("click", ".add-popup-tab",function(){
        var el = $(this)
        var target = el.data("view");
        var common = $(".add-popup-tab");
        var target_common = $(".add-notesordoc-content.enabled");
        change_tabs(el, common, target, target_common);
    });


    /* Document main Tab click */
    $(document).on("click", ".document-popup-tab",function(){
        var el = $(this)
        var target = el.data("view");
        var common = $(".document-popup-tab");
        var target_common = $(".document-popup-content-wrap.enabled");
        if(target){
            change_tabs(el, common, target, target_common);
        }
        
    });

        /* resource main Tab click */
    $(document).on("click", ".resource-popup-tab",function(){
        var el = $(this)
        var target = el.data("view");
        var common = $(".resource-popup-tab");
        var target_common = $(".resource-popup-content-wrap.enabled");
        if(target){
            change_tabs(el, common, target, target_common);
        }
        
    });

    /* Add notes Tab click*/
    $(document).on("click", ".note-tab-list",function(){
        var el = $(this)
        var target = el.data("tab");
        var common = $(".note-tab-list");
        var target_common = $(".note-tab-box.enabled");
        if(target){
            change_tabs(el, common, target, target_common);
        }
        
    });

    /* Accordion functionality*/
    $('.ss-button').on('click',function() {
        var el = $(this);
        if(el.next('.ss-content').is(":visible")) {
        $('.ss-content.enabled').slideUp(200, function(){
            $('.ss-button .accordion-title').removeClass("active");
            $('.ss-content').removeClass("enabled");
        });
        
        } else {
        $('.ss-content.enabled').slideUp(200,function(){
            
        });
        
        $('.ss-content').removeClass("enabled");
        $('.ss-button .accordion-title').removeClass("active");
        el.find(".accordion-title").addClass("active");
        el.next('.ss-content').addClass("enabled");
        el.next('.ss-content').slideDown(200);
        
        }
    });
        
});
/* Add Document popup scroll*/
function document_popup_scroll(){
    $(".document-popup .popup-content").perfectScrollbar();
}
/*Change add notes and attachment tabs*/
function change_tabs(el, common, target, target_common, callback){ 
    if(!el.hasClass("active")){
        $(target_common).fadeOut(300, function(){
            $(target).fadeIn(300);
            $(target_common).removeClass("enabled");
            $(target).addClass("enabled");
        });
        $(common).removeClass("active");
        el.addClass("active");
        if(callback){
            callbak();
        }        
    }
}
/* Things to be initialized*/
function init(){
    /* Initilization for popup inner elements animation*/
    
    TweenMax.set($(".k-popup").find(".anim-lr"),{
        opacity:0,
        x:50
    });
    TweenMax.set($(".k-popup").find(".anim-bt"),{
        opacity:0,
        y:50
    });
    TweenMax.set($(".k-popup").find(".anim-tb"),{
        opacity:0,
        y:-50
    });
    TweenMax.set(".popup-cover",{
        x: win_w
        
    });
}
/* Popup Open function */
function popup_open(el){

    TweenMax.to(".popup-cover", 0.4, {
        x: 0,
        display:"block",
        force3D: !0,
        ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
        onComplete:function(){
            TweenMax.to(".popup-cover", 0.7, {
                x: -win_w,
                force3D: !0,
                ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
                onStart:function(){
                    $(el).show();
                    TweenMax.staggerTo($(el).find(".anim-tb"), 0.6, {
                        delay: 0.3,
                        y: 0,
                        opacity: 1,
                        force3D: !0,
                        ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                    },0.1);
                    TweenMax.staggerTo($(el).find(".anim-lr"), 0.6, {
                        delay: 0.3,
                        x: 0,
                        opacity: 1,
                        force3D: !0,
                        ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                    },0.1);
                    TweenMax.staggerTo($(el).find(".anim-bt"), 0.6, {
                        delay: 0.3,
                        y: 0,
                        opacity: 1,
                        force3D: !0,
                        ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                    },0.1);
                },
                onComplete:function(){

                    TweenMax.set(".popup-cover", {
                        x: win_w,
                        display:"none"
                    });
                }
            });
        }
    });
}

/* Popup Close function */
function popup_close(){
    TweenMax.to(".popup-cover", 0.4, {
        x: 0,
        display:"block",
        force3D: !0,
        ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
        onComplete:function(){
            TweenMax.to(".popup-cover", 0.7, {
                x: -win_w,
                force3D: !0,
                ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
                onStart:function(){
                    $(".k-popup").hide();
                    TweenMax.set($(".k-popup").find(".anim-lr"),{
                        opacity:0,
                        x:50
                    });
                    TweenMax.set($(".k-popup").find(".anim-bt"),{
                        opacity:0,
                        y:50
                    });
                    TweenMax.set($(".k-popup").find(".anim-tb"),{
                        opacity:0,
                        y:-50
                    });
                },
                onComplete:function(){

                    TweenMax.set(".popup-cover", {
                        x: win_w,
                        display:"none"
                    });
                }
            });
        }
    });
}