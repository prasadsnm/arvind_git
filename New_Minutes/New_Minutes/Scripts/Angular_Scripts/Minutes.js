﻿var module = angular.module('minutesApp', [ 'ngRoute', 'slick', 'angular.filter', 'ui.select', 'ui.bootstrap','ui.bootstrap.datetimepicker', 'ui.calendar', 'ngSanitize', 'ngStorage', 'angularFileUpload', 'checklist-model', 'angularjs-dropdown-multiselect', 'ngIdle', 'ngTouch', 'ui.grid', 'ui.grid.exporter', 'ui.grid.selection', 'ui.grid.grouping', 'ui.grid.pagination', 'ui.grid.resizeColumns', 'ui.grid.moveColumns', 'ui.grid.treeView', 'ui.grid.expandable', 'ui.grid.edit', 'angular-nicescroll', 'rzModule', 'lazy-scroll','ui.grid.pinning','ui.grid.autoResize']);
module.constant('_',
    window._
);
var permissionList;
var Build="local";

var BASE_API_URL = "http://localhost:50691/api/";
var CLIENT_URL = "http://localhost:49532/";



String.prototype.splice = function (idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

module.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        // $locationProvider.html5Mode(true);
        $routeProvider.
       
       

                    when('/TimeSheet/AddTimeSheet', {
                        templateUrl: '/TimeSheet/AddTimeSheet'
                    }).
                       when('/TimeSheet/ManageTimeSheet', {
                           templateUrl: '/TimeSheet/ManageTimeSheet'
                       }).
             when('/TimeSheet/ForApproval', {
                 templateUrl: '/TimeSheet/ForApproval'
             }).
             when('/TimeSheet/Approved', {
                 templateUrl: '/TimeSheet/Approved'
             }).
                      when('/TimeSheet/Status', {
                          templateUrl: '/TimeSheet/Status'
                      }).

        otherwise({ redirectTo: '/' });
        
 //   $httpProvider.interceptors.push('AuthHttpResponseInterceptor');
}]);



module.filter('limit', function () {
    return function (text, length) {
      //  alert(text);
        if (text == undefined)
        {
            return null;
        }
        else
        {
            if (text.length > length) {
                return text.substr(0, length) + "...";
            }
            return text;
        }
    }
});

module.directive('tooltip', function ($document, $compile) {
   
    return {
       
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {

            var tip = $compile('<div ng-class="tipClass" >{{text}}<div class="tooltip-arrow"></div></div>')(scope),
                tipClassName = 'tooltip',
                tipActiveClassName = 'tooltip-show';

            scope.tipClass = [tipClassName];
            scope.text = attrs.tooltip;

            if (attrs.tooltipPosition) {
                scope.tipClass.push('tooltip-' + attrs.tooltipPosition);
            }
            else {
                scope.tipClass.push('tooltip-down');
            }
            $document.find('body').append(tip);

            element.bind('mouseover', function (e) {
                tip.addClass(tipActiveClassName);

                var pos = e.target.getBoundingClientRect(),
                    offset = tip.offset(),
                    tipHeight = tip.outerHeight(),
                    tipWidth = tip.outerWidth(),
                    elWidth = pos.width || pos.right - pos.left,
                    elHeight = pos.height || pos.bottom - pos.top,
                    tipOffset = 10;

                if (tip.hasClass('tooltip-right')) {
                    offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
                    offset.left = pos.right + tipOffset;
                }
                else if (tip.hasClass('tooltip-left')) {
                    offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
                    offset.left = pos.left - tipWidth - tipOffset;
                }
                else if (tip.hasClass('tooltip-down')) {
                    offset.top = pos.top + elHeight + tipOffset;
                    offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
                }
                else {
                    offset.top = pos.top - tipHeight - tipOffset;
                    offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
                }

                tip.offset(offset);
            });

            element.bind('mouseout', function () {
                tip.removeClass(tipActiveClassName);
            });

            tip.bind('mouseover', function () {
                tip.addClass(tipActiveClassName);
            });

            tip.bind('mouseout', function () {
                tip.removeClass(tipActiveClassName);
            });


        }
    }
});


module.directive("tooltip", function () {
    return {
        link: function (scope, element, attrs) {

            $(element).on("mouseover", function () {
                $(this).append("<span>" + attrs.tooltip + "</span>");
            });

            $(element).on("mouseout", function () {
                $(this).find("span").remove();
            });

            scope.$on("$destroy", function () {
                $(element).off("mouseover");
                $(element).off("mouseout");
            });
        }
    };
});


