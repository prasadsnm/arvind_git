﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace New_Minutes_API.Layer_Service.Classes
{
    public static class Globals
    {   //this is for local development
        //public static bool isStaging = false; public const string BASE_URL = "http://localhost:49532";
        //this is for deployment
        //public static bool isStaging = true; public const string BASE_URL = "http://km.silpisoft.com";
        //public static bool isStaging = true; public const string BASE_URL = "http://minutes1.silpisoft.com";
        //public static bool isStaging = true; public const string BASE_URL = "http://koordimate.silpisoft.com";
        public static bool isStaging = true; public const string BASE_URL = "*";
        //public static bool isStaging = true; public const string BASE_URL = "http://akha.koordimate.com";
        public const string UPLOAD_FOLDER_URL = "/Data";
    }
    /*
     *Changed files while deploying to server
     1.globals class in api
     2.globals class in client 
     3.minutes.js in client
     4.web.config in api 
     */
}