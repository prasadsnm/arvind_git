﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using New_Minutes_API.Models;


namespace New_Minutes_API.Layer_Service
{
    interface IDbCrudOperations
    {
        IQueryable<object> GetAllRecordsFromDataBase();
        IQueryable<object> GetRecordFromDataBase(int id);
        IEnumerable<object> InsertIntoDataBase(object obj);
           //IEnumerable<object> PreMeetingInsertIntoDataBase(CreateMeetingInfo meetingObj);
    }
}
