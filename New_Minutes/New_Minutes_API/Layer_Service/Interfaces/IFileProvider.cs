﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace New_Minutes_API.Layer_Service.Interfaces
{
    public interface IFileProvider
    {
        bool Exists(string name);
        System.IO.FileStream Open(string name);
        long GetLength(string name);
    }
}
