﻿using New_Minutes_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Linq;
//using CryptCall;

namespace New_Minutes_API
{


    public class WebApiApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
           /* License lic = new License();
            //get hardware key
            string hwKey = lic.HardwareKey();
           
            //check if database has license keys
            string license_key;
            using (var context = new minutesEntities(false))
            {
                 var license = context.license_hash.FirstOrDefault();
                if (license != null)
                {
                    license_key = license.license_key;
                }
                else
                {
                    license_key = "";
                }
            }

            
            

            string xmlStoreEncKey;
            if (license_key == null || license_key.Length == 0)
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = ("    ");
                settings.CloseOutput = true;
                settings.OmitXmlDeclaration = true;
                xmlStoreEncKey = lic.GetEncryptedHwKey();
                using (XmlWriter writer = XmlWriter.Create(Server.MapPath("~/Data/keys_hash.xml"), settings))
                {
                    writer.WriteStartElement("License");
                    writer.WriteElementString("Key", xmlStoreEncKey);
                    writer.WriteEndElement();
                    writer.Flush();
                }
            }
            else if (license_key != null && license_key.Length!= 0)
            {
                //get encrypted value from xml
                XElement xmlroot = XElement.Parse(license_key);
                string enckey = ((System.Xml.Linq.XElement)(xmlroot.FirstNode)).Value;

                if (lic.ValidateKey(enckey))
                {*/

                    AreaRegistration.RegisterAllAreas();
                    GlobalConfiguration.Configure(WebApiConfig.Register);
                    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                    RouteConfig.RegisterRoutes(RouteTable.Routes);
                    BundleConfig.RegisterBundles(BundleTable.Bundles);
              /*  }
            }
            else
            {
                return;
            } */
            
        
        }
        
    }
}
