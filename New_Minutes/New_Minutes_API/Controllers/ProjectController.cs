﻿using New_Minutes_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using New_Minutes_API.Layer_Service.Classes;
using New_Minutes_API.Layer_Service;
using System.Web.Http.Cors;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data.Entity;

namespace New_Minutes_API.Controllers
{
    // [AuthorizationRequired]
    [EnableCors(origins: Globals.BASE_URL, headers: "*", methods: "*")]
    public class ProjectController : ApiController
    {
        public IQueryable getProjectsByEmployeeID(int Employeeid)
        {
            using (var context = new minutesEntities())
            {
                var result = from t in context.tasks.Where(w => w.record_status == "A" && w.task_owner_id == Employeeid)
                             join st in context.lkp_status.Where(w => w.record_status == "A" && w.status_short_desc != "TCP" && w.status_category == "TASK") on t.task_status_id equals st.status_id
                             join p in context.projects.Where(w => w.record_status == "A") on t.project_id equals p.project_id
                             //join pr in context.project_resources.Where(w => w.record_status == "A" && w.resource_short_role_desc == "PPM") on p.project_id equals pr.project_id
                             //join emp in context.employees.Where(w => w.record_status == "A") on pr.employee_id equals emp.employee_id
                             select new
                             {
                                 p.project_id,
                                 p.project_name,





                             };


                return result.ToList().Distinct().OrderBy(y => y.project_name).AsQueryable();
            }


        }

        public IQueryable InsertEmployee(employee emp)
        {
            int success = 0;
            using (var context = new minutesEntities())
            {

                context.employees.Add(emp);
                success = context.SaveChanges();
            }
            List<object> list = new List<object>() { success > 0 ? ResultProcesser.ProcessResult(true) : ResultProcesser.ProcessResult(false) };
            if (success > 0)
            {
                Dictionary<string, int> result = new Dictionary<string, int>();
                result.Add("dept_id", emp.employee_id);
                list.Add(result);
            }
            return list.AsQueryable();
        }
        public IQueryable UpdateEmployee(employee emp)
        {
            int success = 0;
            using (var context = new minutesEntities())
            {
                context.Entry(emp).State = EntityState.Modified;  
            }
            List<object> list = new List<object>() { success > 0 ? ResultProcesser.ProcessResult(true) : ResultProcesser.ProcessResult(false) };
            return list.AsQueryable();
        }


    
    }
}

